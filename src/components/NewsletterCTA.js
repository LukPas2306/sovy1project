import React from "react";

const NewsletterCTA = () => (
  <div
    data-w-id='2137cadc-f27c-f8b8-d4bf-ae8d48effbd1'
    className='content cc-subscribe-form'
  >
    <div className='subscribe-wrap'>
      <div className='wrap cc-subscribe-form'>
        <div className='footer-subheadline'>
          <div className='subscribe-form-deco-line' />
          <div className='subscribe-form-deco-line' />
        </div>
        <div className='subscribe-headline'>Get our updates</div>
        <div className='form-block w-form'>
          <form
            id='wf-form-Subscribe-Form'
            name='wf-form-Subscribe-Form'
            data-name='Subscribe Form'
            className='form'
          >
            <input
              type='text'
              className='text-field-dark cc-subscribe-input w-input'
              maxLength={256}
              name='Customer-s-Name'
              data-name="Customer's Name"
              placeholder='customer@bookshop.com'
              id='Customer-s-Name'
            />
            <input
              type='submit'
              defaultValue='Subscribe'
              data-wait='Please wait...'
              className='white-button w-button'
            />
          </form>
          <div className='success-message w-form-done'>
            <div className='success-message-wrap'>
              <img
                src='images/Success-Icon-White_1Success-Icon-White.png'
                width={33}
                alt
                className='newsletter-success-icon'
              />
              <div className='newsletter-success-text'>
                Thank you! Your submission has been received!
              </div>
            </div>
          </div>
          <div className='error-message w-form-fail'>
            <div>Oops! Something went wrong while submitting the form.</div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default NewsletterCTA;
