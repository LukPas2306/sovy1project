import React from "react";
import { Router } from "@reach/router";
import HomePage from "./HomePage";
import AboutPage from "./AboutPage";

export default () => (
  <Router>
    <HomePage path='/' />
    <AboutPage path='/about' />
  </Router>
);
