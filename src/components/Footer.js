import React from "react";

const Footer = () => (
  <div className='content'>
    <div className='wrap'>
      <div
        data-w-id='4c625f5d-597c-b93b-cc62-5e4ebf27d696'
        className='w-layout-grid footer'
      >
        <div
          id='w-node-5e4ebf27d69c-50f9fcff'
          className='label cc-footer-section-headline w-inline-block'
        >
          Menu
        </div>
        <div id='w-node-5e4ebf27d69e-50f9fcff' className='footer-grid-column'>
          <a href='index.html' className='footer-link w--current'>
            Home
          </a>
          <a href='products.html' className='footer-link'>
            Our Products
          </a>
          <a href='about.html' className='footer-link'>
            About
          </a>
          <a href='contact.html' className='footer-link'>
            Contact
          </a>
        </div>
        <div
          id='w-node-5e4ebf27d6a9-50f9fcff'
          className='label cc-footer-section-headline w-inline-block'
        >
          Follow Us
        </div>
        <div id='w-node-5e4ebf27d6ab-50f9fcff' className='footer-grid-column'>
          <a
            href='https://www.facebook.com/'
            target='_blank'
            className='footer-link'
          >
            Facebook
          </a>
          <a
            href='https://www.instagram.com/'
            target='_blank'
            className='footer-link'
          >
            Instagram
          </a>
          <a
            href='https://www.pinterest.com/'
            target='_blank'
            className='footer-link'
          >
            Pinterest
          </a>
          <a
            href='https://twitter.com/'
            target='_blank'
            className='footer-link'
          >
            Twitter
          </a>
        </div>
        <div
          id='w-node-5e4ebf27d6b4-50f9fcff'
          className='label cc-footer-section-headline w-inline-block'
        >
          Contact Us
        </div>
        <div id='w-node-5e4ebf27d6b6-50f9fcff' className='footer-grid-column'>
          <p className='small-paragraph'>We’re Always Happy to Help</p>
          <a href='mailto:us@coffeestyle.io' className='footer-email-link'>
            books@shop.com
          </a>
        </div>
      </div>
    </div>
  </div>
);

export default Footer;
