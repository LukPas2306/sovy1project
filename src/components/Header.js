import React from "react";

const Header = () => (
  <div id='w-node-c269a2f6ad2b-a2f6ad2b' className='menu'>
    <div
      data-collapse='medium'
      data-animation='default'
      data-duration={400}
      data-easing='ease-out'
      className='wrap cc-menu w-nav'
    >
      <a href='index.html' className='brand w-nav-brand w--current'>
        <img
          src='images/inn.png'
          width={200}
          srcSet='images/inn-p-500.png 500w, images/inn.png 642w'
          sizes='(max-width: 479px) 100vw, 200px'
          alt=''
          className='logo-image'
        />
      </a>
      <nav role='navigation' className='nav-menu w-nav-menu'>
        <a
          href='index.html'
          className='menu-item-wrap w-inline-block w--current'
        >
          <div className='menu-item'>Home</div>
          <div
            data-w-id='e88a0756-985d-3e65-9a57-c269a2f6ad33'
            className='menu-item-deco-line'
          />
        </a>
        <a href='products.html' className='menu-item-wrap w-inline-block'>
          <div className='menu-item'>Our Products</div>
          <div className='menu-item-deco-line' />
        </a>
        <a href='about.html' className='menu-item-wrap w-inline-block'>
          <div className='menu-item'>About</div>
          <div className='menu-item-deco-line' />
        </a>
        <a href='contact.html' className='menu-item-wrap w-inline-block'>
          <div className='menu-item'>Contact</div>
          <div className='menu-item-deco-line' />
        </a>
      </nav>
      <div
        data-node-type='commerce-cart-wrapper'
        data-open-product
        data-wf-cart-type='rightSidebar'
        data-wf-cart-query
        data-wf-page-link-href-prefix
        className='w-commerce-commercecartwrapper'
      >
        <a
          href='#'
          data-node-type='commerce-cart-open-link'
          className='w-commerce-commercecartopenlink cart-button w-inline-block'
        >
          <img
            src='images/Cart-Icon_1Cart-Icon.png'
            width={16}
            alt
            className='cart-icon'
          />
          <div className='uppercase-text w-inline-block'>Cart</div>
          <div className='w-commerce-commercecartopenlinkcount item-count'>
            0
          </div>
          <div className='menu-item-deco-line' />
        </a>
        <div
          data-node-type='commerce-cart-container-wrapper'
          style={{ display: "none" }}
          className='w-commerce-commercecartcontainerwrapper w-commerce-commercecartcontainerwrapper--cartType-rightSidebar cart-wrapper'
        >
          <div
            data-node-type='commerce-cart-container'
            className='w-commerce-commercecartcontainer cart-container'
          >
            <div className='w-commerce-commercecartheader cart-header'>
              <h4 className='w-commerce-commercecartheading cart-heading'>
                Your Cart
              </h4>
              <a
                href='#'
                data-node-type='commerce-cart-close-link'
                className='w-commerce-commercecartcloselink close-button w-inline-block'
              >
                <img
                  src='images/Cart-Close-Icon_1Cart-Close-Icon.png'
                  width={13}
                  alt
                  className='cart-close-icon'
                />
              </a>
            </div>
            <div className='w-commerce-commercecartformwrapper'>
              <form
                data-node-type='commerce-cart-form'
                style={{ display: "none" }}
                className='w-commerce-commercecartform'
              >
                <div
                  className='w-commerce-commercecartlist cart-list'
                  data-wf-collection='database.commerceOrder.userItems'
                  data-wf-template-id='wf-template-fe766061-df28-ca50-5e20-ce56ac75e811'
                />
                <div className='w-commerce-commercecartfooter cart-footer'>
                  <div className='w-commerce-commercecartlineitem cart-line-item'>
                    <div className='cart-total'>Subtotal</div>
                    <div className='w-commerce-commercecartordervalue cart-order-value' />
                  </div>
                  <div className='checkout-actions'>
                    <div data-node-type='commerce-cart-quick-checkout-actions'>
                      <a
                        data-node-type='commerce-cart-apple-pay-button'
                        style={{
                          display: "none",
                          backgroundImage:
                            "-webkit-named-image(apple-pay-logo-white)",
                          backgroundSize: "100% 50%",
                          backgroundPosition: "50% 50%",
                          backgroundRepeat: "no-repeat"
                        }}
                        className='w-commerce-commercecartapplepaybutton'
                      >
                        <div />
                      </a>
                      <a
                        data-node-type='commerce-cart-quick-checkout-button'
                        style={{ display: "none" }}
                        className='w-commerce-commercecartquickcheckoutbutton'
                      >
                        <svg
                          className='w-commerce-commercequickcheckoutgoogleicon'
                          xmlns='http://www.w3.org/2000/svg'
                          xmlnsXlink='http://www.w3.org/1999/xlink'
                          width={16}
                          height={16}
                          viewBox='0 0 16 16'
                        >
                          <defs>
                            <polygon
                              id='google-mark-a'
                              points='0 .329 3.494 .329 3.494 7.649 0 7.649'
                            />
                            <polygon
                              id='google-mark-c'
                              points='.894 0 13.169 0 13.169 6.443 .894 6.443'
                            />
                          </defs>
                          <g fill='none' fillRule='evenodd'>
                            <path
                              fill='#4285F4'
                              d='M10.5967,12.0469 L10.5967,14.0649 L13.1167,14.0649 C14.6047,12.6759 15.4577,10.6209 15.4577,8.1779 C15.4577,7.6339 15.4137,7.0889 15.3257,6.5559 L7.8887,6.5559 L7.8887,9.6329 L12.1507,9.6329 C11.9767,10.6119 11.4147,11.4899 10.5967,12.0469'
                            />
                            <path
                              fill='#34A853'
                              d='M7.8887,16 C10.0137,16 11.8107,15.289 13.1147,14.067 C13.1147,14.066 13.1157,14.065 13.1167,14.064 L10.5967,12.047 C10.5877,12.053 10.5807,12.061 10.5727,12.067 C9.8607,12.556 8.9507,12.833 7.8887,12.833 C5.8577,12.833 4.1387,11.457 3.4937,9.605 L0.8747,9.605 L0.8747,11.648 C2.2197,14.319 4.9287,16 7.8887,16'
                            />
                            <g transform='translate(0 4)'>
                              <mask id='google-mark-b' fill='#fff'>
                                <use xlinkHref='#google-mark-a' />
                              </mask>
                              <path
                                fill='#FBBC04'
                                d='M3.4639,5.5337 C3.1369,4.5477 3.1359,3.4727 3.4609,2.4757 L3.4639,2.4777 C3.4679,2.4657 3.4749,2.4547 3.4789,2.4427 L3.4939,0.3287 L0.8939,0.3287 C0.8799,0.3577 0.8599,0.3827 0.8459,0.4117 C-0.2821,2.6667 -0.2821,5.3337 0.8459,7.5887 L0.8459,7.5997 C0.8549,7.6167 0.8659,7.6317 0.8749,7.6487 L3.4939,5.6057 C3.4849,5.5807 3.4729,5.5587 3.4639,5.5337'
                                mask='url(#google-mark-b)'
                              />
                            </g>
                            <mask id='google-mark-d' fill='#fff'>
                              <use xlinkHref='#google-mark-c' />
                            </mask>
                            <path
                              fill='#EA4335'
                              d='M0.894,4.3291 L3.478,6.4431 C4.113,4.5611 5.843,3.1671 7.889,3.1671 C9.018,3.1451 10.102,3.5781 10.912,4.3671 L13.169,2.0781 C11.733,0.7231 9.85,-0.0219 7.889,0.0001 C4.941,0.0001 2.245,1.6791 0.894,4.3291'
                              mask='url(#google-mark-d)'
                            />
                          </g>
                        </svg>
                        <svg
                          className='w-commerce-commercequickcheckoutmicrosofticon'
                          xmlns='http://www.w3.org/2000/svg'
                          width={16}
                          height={16}
                          viewBox='0 0 16 16'
                        >
                          <g fill='none' fillRule='evenodd'>
                            <polygon fill='#F05022' points='7 7 1 7 1 1 7 1' />
                            <polygon
                              fill='#7DB902'
                              points='15 7 9 7 9 1 15 1'
                            />
                            <polygon
                              fill='#00A4EE'
                              points='7 15 1 15 1 9 7 9'
                            />
                            <polygon
                              fill='#FFB700'
                              points='15 15 9 15 9 9 15 9'
                            />
                          </g>
                        </svg>
                        <div>Pay with browser</div>
                      </a>
                    </div>
                    <a
                      href='checkout.html'
                      value='Continue to Checkout'
                      data-node-type='cart-checkout-button'
                      className='w-commerce-commercecartcheckoutbutton white-button cc-product-button w-inline-block'
                      data-loading-text='Hang Tight...'
                    >
                      Continue to Checkout
                    </a>
                  </div>
                </div>
              </form>
              <div className='w-commerce-commercecartemptystate'>
                <div className='empty-cart-wrap'>
                  <img
                    src='images/Empty-Cart-Icon_1Empty-Cart-Icon.png'
                    width={30}
                    alt
                    className='empty-cart-icon'
                  />
                  <div className='empty-cart-headline'>Your Cart is Empty</div>
                  <p className='paragraph cc-empty-cart'>
                    It is a paradisematic country, in which roasted parts of
                    sentences fly into your mouth. Even the all-powerful.
                  </p>
                </div>
              </div>
              <div
                style={{ display: "none" }}
                data-node-type='commerce-cart-error'
                className='w-commerce-commercecarterrorstate error-state'
              >
                <div
                  className='w-cart-error-msg'
                  data-w-cart-quantity-error='Product is not available in this quantity.'
                  data-w-cart-checkout-error='Checkout is disabled on this site.'
                  data-w-cart-general-error='Something went wrong when adding this item to the cart.'
                >
                  Product is not available in this quantity.
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='menu-button w-nav-button'>
        <img
          src='images/Menu-Icon_1Menu-Icon.png'
          width={20}
          alt
          className='menu-icon'
        />
      </div>
    </div>
  </div>
);

export default Header;
