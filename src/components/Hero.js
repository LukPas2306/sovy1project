import React from "react";

const Hero = () => (
  <div className='header-section'>
    <div className='header-image'>
      <div className='header-content'>
        <div className='header-text'>
          <div className='header-headline-wrap'>
            <div className='label cc-cover-label w-inline-block'>
              The book inn. Place where you can meet your kindred book.
            </div>
            <div className='header-headline'>
              THERE IS NO SUCH THING AS TOO MANY BOOKS.
            </div>
            <p className='big-paragraph cc-cover-paragraph'>❤</p>
          </div>
          <a href='products.html' className='white-button w-inline-block'>
            <div>Explore Our Products</div>
          </a>
        </div>
      </div>
    </div>
  </div>
);

export default Hero;
