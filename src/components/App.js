import React from "react";
import Layout from "./Layout";
import Router from "./Router";

const App = () => (
  <Layout>
    <Router />
  </Layout>
);

export default App;
