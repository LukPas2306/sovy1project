import React from "react";
import Hero from "./Hero";
import NewsletterCTA from "./NewsletterCTA";

const Homepage = () => (
  <>
    <Hero />
    <NewsletterCTA />
  </>
);

export default Homepage;
